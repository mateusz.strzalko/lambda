terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region  = "eu-central-1"
}

resource "aws_s3_bucket" "s3_test" {
  bucket = "my-tf-test-s3-${var.workspace}"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}
resource "aws_iam_role" "role_lambda_aws_cli" {
  name = "my-lambda-function-ROLE-${var.workspace}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_lambda_function" "lambda_function" {
  function_name = "my-lambda-function-${var.workspace}"
  handler      = "org.springframework.cloud.function.adapter.aws.FunctionInvoker::handleRequest"
  runtime      = "java17"

  source_code_hash = filebase64("${path.module}/target/lambda-0.0.1-SNAPSHOT-aws.jar")
  filename = "${path.module}/target/lambda-0.0.1-SNAPSHOT-aws.jar"
  role = aws_iam_role.role_lambda_aws_cli.arn

}


#
#resource "aws_step_functions_state_machine" "step_function" {
#  name     = "my-step-function"
#  role_arn = "arn:aws:iam::<account_id>:role/your-existing-step-function-role"
#
#  definition = <<EOF
#{
#  "Comment": "My Step Function",
#  "StartAt": "InvokeLambda",
#  "States": {
#    "InvokeLambda": {
#      "Type": "Task",
#      "Resource": "${aws_lambda_function.lambda_function.arn}",
#      "End": true
#    }
#  }
#}
#EOF
#
#  depends_on = [
#    aws_iam_policy_attachment.step_function_attachment
#  ]
#}
#
#resource "aws_iam_policy_attachment" "step_function_attachment" {
#  name       = "step_function_attachment"
#  policy_arn = "arn:aws:iam::aws:policy/AWSStepFunctionsFullAccess"
#  roles      = [aws_iam_role.lambda_exec.arn]
#}
