package com.example.lambda.function;

import com.example.lambda.dto.RandomNumberDto;
import org.springframework.cglib.core.internal.Function;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component("GenerateRandomNumber-Function")
public class Example1Function implements Function<Map<String, Object>, RandomNumberDto> {


    @Override
    public RandomNumberDto apply(Map<String, Object> key) {
        int randomNumber = (int) (Math.random() * 10);
        return new RandomNumberDto(randomNumber);
    }

}

