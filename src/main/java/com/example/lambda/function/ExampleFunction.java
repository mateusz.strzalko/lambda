package com.example.lambda.function;


import lombok.extern.slf4j.Slf4j;
import org.springframework.cglib.core.internal.Function;
import org.springframework.stereotype.Component;

import java.util.Map;
@Slf4j
@Component("Example-Function")
public class ExampleFunction implements Function<Map<String, Object>, Map<String, Object>> {
    @Override
    public Map<String, Object> apply(Map<String, Object> key) {
        log.info("message: {}", key);
        key.put("lambda0", "lambda0");

        return key;
    }

    //zaimplementowac interfejs funkcujny


}
